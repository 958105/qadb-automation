package IO_Resources;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

/**
 * Created by 958105 on 31/03/2018.
 */
public class Filereaderwrite {
    /* File Variables */
    public static String Service_Partner_name, CSA_name , SOW_name, ExchCode,  Task_aud_stmt, Freetext;
    public static String Chorus_task_id;
    public static String SOW_SP_ID, Job_Q_ID, SOW_Task_id, Audit_ID, Audit_Task_id;
    Date date = new Date();

    public Filereaderwrite()
    {
        Service_Partner_name = null;
        CSA_name = null;
        SOW_name = null;
        SOW_SP_ID = null;
        Job_Q_ID = null;
        SOW_Task_id = null;
        Audit_ID = null;
        Audit_Task_id = null;
    }

    public void ReadExcelFile() throws IOException, InvalidFormatException
    {
        //Create an object of File class to open xlsx file
        FileInputStream fis=new FileInputStream("H:\\Documents\\QADB Automation Data Sheet.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet inputSheet = wb.getSheet("Input");
        Row row=inputSheet.getRow(1);
        DataFormatter formatter = new DataFormatter();
        if (row != null)
        {
            Cell cell1 = row.getCell(0);
            Cell cell2=row.getCell(1);
            Cell cell3=row.getCell(2);
            Cell cell4=row.getCell(3);
            Cell cell5=row.getCell(4);
            Cell cell6=row.getCell(5);
            Cell cell7=row.getCell(6);
            Service_Partner_name = cell1.getStringCellValue();
            CSA_name = cell2.getStringCellValue();
            SOW_name = cell3.getStringCellValue();
            ExchCode = cell4.getStringCellValue();
            Chorus_task_id = formatter.formatCellValue(cell5);
            Task_aud_stmt = cell6.getStringCellValue();
            Freetext = cell7.getStringCellValue();
            System.out.println(Service_Partner_name + " " + CSA_name + " " + SOW_name + " " + ExchCode + " " + Chorus_task_id + " " + Task_aud_stmt + " " + Freetext   );
        }
        fis.close();
    }

    /*public void writeExcelFile( Filereaderwrite filehandler) throws IOException, InvalidFormatException
    {
        FileInputStream fis=new FileInputStream("H:\\Documents\\QADB Automation Data Sheet.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        XSSFSheet inputSheet = wb.getSheet("Input");

        XSSFRow rowindex = inputSheet.getRow(1);
        XSSFCell cell7 , cell8, cell9, cell10, cell11;

        cell7 = rowindex.createCell(7);
        cell7.setCellValue(saleobj.SOW_SP_ID);

        cell8 = rowindex.createCell(8);
        cell8.setCellValue(saleobj.Job_Q_ID);

        cell9 = rowindex.createCell(9);
        cell9.setCellValue(saleobj.SOW_Task_id);

        cell10 = rowindex.createCell(10);
        cell10.setCellValue(saleobj.Audit_Task_id);

        cell11 = rowindex.createCell(11);
        cell11.setCellValue(saleobj.Audit_ID);

        fis.close();

        FileOutputStream fileout = new FileOutputStream("H:\\Documents\\QADB Automation Data Sheet.xlsx");

        wb.write(fileout);
        wb.close();
        fileout.close();

    }*/
}
