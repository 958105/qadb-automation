package IO_Resources;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * Created by 958105 on 31/03/2018.
 */
public class DateFunctions {
    static Date date = new Date();
    public static String backdate()
    {
        LocalDateTime localDateTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime = localDateTime.minusDays(1);
        Date currentDateminusThreeDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        String curformat = new SimpleDateFormat("dd/MM/yyyy").format(currentDateminusThreeDay);
        return curformat;
    }
    public static String Postdate() {
        LocalDateTime localDateTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime = localDateTime.plusDays(5);
        Date currentDateplussevenDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        String curformat = new SimpleDateFormat("dd/MM/yyyy").format(currentDateplussevenDay);
        return curformat;
    }
}
