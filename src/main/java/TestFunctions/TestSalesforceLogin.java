package TestFunctions;

/**
 * Created by 958105 on 23/03/2018.
 */
import Pages.*;
import IO_Resources.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import Pages.LoginPage;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.xpath.operations.Neg;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class TestSalesforceLogin {
    WebDriver driver;
    LoginPage objLogin;
    SalesforceHomePage objHomePage;
    StatementofWorkPage objsowpage;
    SOWServicePartnerPage sowspobj;
    Filereaderwrite filehandler;
    AuditTaskPage objaudittaskpage;

    By homePageUserName = By.id("userNavLabel");
    By logoutAction = By.linkText("Logout");

    @BeforeTest
    public void setup() throws IOException, InvalidFormatException {
        System.setProperty("webdriver.gecko.driver", "H:\\Chorus Works\\PAckage\\Selenium\\geckodriver-v0.18.0-win64\\geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.get("https://test.salesforce.com/");

        try {
            filehandler = new Filereaderwrite();
            filehandler.ReadExcelFile();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
    }

    @Test(priority=0)
    public void Login_test() {

        //Create Login Page object
        objLogin = new LoginPage(driver);

        //Verify login page title
        String loginPageTitle = objLogin.getLoginTitle();
        System.out.println("Login Page Title: " + loginPageTitle);

        Assert.assertEquals(loginPageTitle, "Login | Salesforce");
        //login to application
        objLogin.loginToSalesforece("chandra.palani@chorus.co.nz.staging", "Thulashiha12#");

        // go the next page
        objHomePage = new SalesforceHomePage(driver);
        //Verify home page
        //System.out.println("User " + objHomePage.getHomePageDashboardUserName().toLowerCase() + " Has Logged on");
        Assert.assertTrue(objHomePage.getHomePageDashboardUserName().toLowerCase().contains("chandra"));

    }

    @Test(priority = 1, dependsOnMethods = {"Login_test"})
    public void CreateStatementofWork() throws InterruptedException {

        // Button Click action for New Statement of work action *
        objHomePage.click_statementofwork();
        driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);

        objsowpage = new StatementofWorkPage(driver);
        // Create new Statement of Work action
        objsowpage.new_click_statementofwork();
        objsowpage.create_new_sow(filehandler);
        Assert.assertTrue(objsowpage.getsowname().contains(filehandler.SOW_name));
    }



    @Test(priority = 2, dependsOnMethods = {"CreateStatementofWork"})
    public void ServicePartnerCreation ()
    {
        objsowpage.click_sow_servicepartner();
        sowspobj = new SOWServicePartnerPage(driver);
        String ServicepartnerName = sowspobj.new_servicepartner_page(filehandler);
        //perform an assert function at some stage
        if (ServicepartnerName != null)
            filehandler.SOW_SP_ID = ServicepartnerName;
        else
            System.out.println("Error in Creating the Service Partner");
        Assert.assertNotEquals(ServicepartnerName,null);
        //Assert.assertFalse(ServicepartnerName.contains(null));
    }

   /* @Test(priority = 3, dependsOnMethods = {"ServicePartnerCreation"})
     public void DeleteStatementofWork_ServicePartner() throws InterruptedException
    {
        Boolean sowsp_found = Boolean.valueOf(objHomePage.sowsp_search_action(filehandler.SOW_SP_ID));
        System.out.println("SOWSP Found value:" + sowsp_found);
        if (sowsp_found)
        sowspobj.delete_SOWSP(filehandler.SOW_SP_ID);

    }
    @Test(priority = 4, dependsOnMethods = {"DeleteStatementofWork_ServicePartner"})
    public void DeleteStatementofWork() throws InterruptedException
    {
        Boolean sow_found = Boolean.valueOf(objHomePage.sow_search_action(filehandler.SOW_name));
        //System.out.println("SOW Found value:" + sow_found);
        if (sow_found)
            objsowpage.sow_delete();

        // have to assert the delete is sucessfull by searching it again; But require the search method to be modified to handle the match not found
        // Assert.assertNotEquals(filehandler.SOW_name,objHomePage.sow_search_action(filehandler.SOW_name), "No Match found");

    } */

    @Test(priority = 5, dependsOnMethods = {"ServicePartnerCreation"})
    public void createJob_Quantities()
    {
        sowspobj.Job_Quantities_sp_click();
        sowspobj.job_quantities_sp_create("2018", "100", "Monthly", filehandler);
    }

    public void createAuditTask(){
        objHomePage.audittask_click();
        objaudittaskpage = new AuditTaskPage(driver);
        objaudittaskpage.audittask_new();
        objaudittaskpage.audittask_create(filehandler);

    }

    @Test(priority = 6, dependsOnMethods = {"createJob_Quantities"})
    public void SalesforceLogout() {

        driver.findElement(homePageUserName).click();
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);


        driver.findElement(logoutAction).click();
        driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);
    }

      /* public static void main(String args[]) throws InterruptedException {

        //Login TC001
        TestSalesforceLogin obj = new TestSalesforceLogin();
        obj.setup();

        //TC002 - Login Verification
        obj.test_Home_Page_Appear_Correct();

        //TC003 - Logout in built
        obj.SalesforceLogout();

    }*/
}
