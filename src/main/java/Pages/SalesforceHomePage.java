package Pages;
import IO_Resources.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

/**
 * Created by 958105 on 23/03/2018.
 */
public class SalesforceHomePage {

    WebDriver driver;
    By homePageUserName = By.id("userNavLabel");
    By logoutAction = By.className("menuButtonMenuLink");
    By StatementofWork = By.id("01r20000000FVSk_Tab");
    By search_input = By.id("phSearchInput");
    By search_btn = By.id("phSearchButton");
    By search_sow_res = By.xpath("//*[@id=\"qaSOW__c_body\"]/table/tbody/tr[2]/th/a");
    By search_sowsp_res  = By.xpath("//*[@id=\"qaStatement_of_Work_Service_Partner__c_body\"]/table/tbody/tr[2]/th/a");
    By search_sowtask_res = By.xpath("//*[@id=\"qaChecksheetTask__c_body\"]/table/tbody/tr[2]/th/a");
    By audittask = By.cssSelector("#\\30 1r20000000HCek_Tab > a");

    public SalesforceHomePage(WebDriver driver) {
        this.driver = driver;
    }

    //Get the User name from Home Page
    public String getHomePageDashboardUserName(){
        return    driver.findElement(homePageUserName).getText();
    }

    // Click Statement of Work
    public void click_statementofwork()
    {
        driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
        driver.findElement(StatementofWork).click();
    }

    public Boolean sow_search_action(String search_string)
    {
        driver.findElement(search_input).sendKeys(search_string);
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        driver.findElement(search_btn).click();
        driver.manage().timeouts().implicitlyWait(800, TimeUnit.MILLISECONDS);
        //workaround to load all the containers; Have to handle this via readystate and Wait for Page Load function

    //Assertion to be added and more functions to be provided; currently i am impelemting for SOW search

        Boolean sow_found = driver.findElement(search_sow_res).isDisplayed();

        if (sow_found) {
            driver.navigate().refresh();
            driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
            driver.findElement(search_sow_res).click();
            driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
        }
        else
            System.out.println(" Object not found");

        return (sow_found);
     }

    public Boolean sowsp_search_action(String searchsp_string)
    {
        driver.findElement(search_input).sendKeys(searchsp_string);
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        driver.findElement(search_btn).click();
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        driver.navigate().refresh();
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        //Assertion to be added and more functions to be provided; currently i am impelemting for SOW search

        Boolean sow_sp_found = driver.findElement(search_sowsp_res).isDisplayed();

        if (sow_sp_found) {
            driver.findElement(search_sowsp_res).click();
            driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
        }
        else
            System.out.println(" Object not found");

        return (sow_sp_found);
    }

    public Boolean sowtask_search_action(String searchsp_string)
    {
        driver.findElement(search_input).sendKeys(searchsp_string);
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        driver.findElement(search_btn).click();
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        driver.navigate().refresh();
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        //Assertion to be added and more functions to be provided; currently i am impelemting for SOW search

        Boolean sowtask_found = driver.findElement(search_sowtask_res).isDisplayed();

        if (sowtask_found) {
            driver.findElement(search_sowsp_res).click();
            driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
        }
        else
            System.out.println(" Object not found");

        return (sowtask_found);
    }


    public void audittask_click(){
        driver.findElement(audittask).click();
        driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
    }
}
