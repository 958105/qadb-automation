package Pages;

import IO_Resources.DateFunctions;
import IO_Resources.Filereaderwrite;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by 958105 on 30/04/2018.
 */
public class StatementofWorkTasksPage {
    WebDriver driver;
    By SOW_name = By.id("CF00N20000002h8e9");
    By start_date =  By.id("00N20000009pzLj");
    By end_date = By.id("00N20000009pzLh");
    By sowtask_save = By.name("save");
    By sowtask_cancel = By.name("cancel");
    By sowtask_saveandnew = By.name("save_new");
    By sowtask_id = By.cssSelector("#Name_ileinner");

    public StatementofWorkTasksPage(WebDriver driver){this.driver = driver;}

    public void sow_task_create(Filereaderwrite filehandler)
    {
        driver.findElement(SOW_name).sendKeys(filehandler.SOW_name);
        String backdate = DateFunctions.backdate();
        String postdate = DateFunctions.Postdate();
        driver.findElement(start_date).sendKeys(backdate);
        driver.findElement(end_date).sendKeys(postdate);

        driver.findElement(sowtask_save).click();
        driver.manage().timeouts().implicitlyWait(1500, TimeUnit.MILLISECONDS);
        filehandler.SOW_Task_id = driver.findElement(sowtask_id).getText();

    }
    public void sow_task_delete(String sowtask_name)
    {

    }
    public void sow_task_clone()
    {

    }
    public void sowtask_approval(String SOWtask_id)
    {


    }
}
