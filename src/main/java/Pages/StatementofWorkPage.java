package Pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import IO_Resources.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by 958105 on 31/03/2018.
 */
public class StatementofWorkPage {
    WebDriver driver;
    By sow_name = By.id("Name");
    By start_date = By.id("00N20000009pzLu");
    By end_date = By.id("00N20000009pzLt");
    By sow_type = By.id("00N7E000001pQT2");
    By network_type = By.id("00N7E000001pQSn");
    By sow_descr = By.id("00N0O00000A9cmj");
    By sow_save_btn = By.name("save");
    By sow_savenew_btn = By.name("save_new");
    By sow_cancel_btn = By.name("cancel");
    By create_sow_name = By.cssSelector("#bodyCell > div.bPageTitle > div.ptBody > div.content > h2");
    By sow_edit = By.cssSelector("#topButtonRow > input:nth-child(3)");
    By sow_delete = By.cssSelector("#topButtonRow > input:nth-child(4)");
    By sow_clone = By.cssSelector("#topButtonRow > input:nth-child(5)");
    By sow_new_btn = By.xpath("//*[@id=\"hotlist\"]/table/tbody/tr/td[2]/input");
    By sow_sp_btn = By.name("new00N20000009pzM6");
    By jobQuantities_btn = By.xpath("//*[@id=\"a0F250000047ZSE_00N20000002h8fa\"]/div[1]/div/div[1]/table/tbody/tr/td[2]/input");

    public StatementofWorkPage(WebDriver driver)
    {
            this.driver = driver;
    }
    public void new_click_statementofwork()
    {
        driver.findElement(sow_new_btn).click();
        driver.manage().timeouts().implicitlyWait(800, TimeUnit.MILLISECONDS);
    }
    public void click_sow_servicepartner()
    {
        driver.findElement(sow_sp_btn).click();
        driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
    }
    public void create_new_sow(Filereaderwrite filehandler) throws InterruptedException {
        driver.findElement(sow_name).sendKeys(filehandler.SOW_name);
        // Start date will be always backdated
        String minusthreedays  = DateFunctions.backdate();
        driver.findElement(start_date).sendKeys(minusthreedays);
        String postdays  = DateFunctions.Postdate();
        driver.findElement(end_date).sendKeys(postdays);
        driver.findElement(sow_descr).sendKeys("Automation Test Description for demo");
        driver.findElement(sow_save_btn).click();
        driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);
    }

    public String getsowname(){
        return driver.findElement(create_sow_name).getText();
    }

    // Action on a newly created or existing SOW
    public void sow_delete(){
        driver.findElement(sow_delete).click();
        Alert alert = driver.switchTo().alert();
        alert.accept();
        driver.manage().timeouts().implicitlyWait(200, TimeUnit.MILLISECONDS);
    }

    public void sow_edit()
    {
       driver.findElement(sow_edit).click();
    }

    public void sow_clone()
    {
      driver.findElement(sow_clone).click();
    }

    public void Job_Quantities_click(){
        driver.findElement(jobQuantities_btn).click();
        driver.manage().timeouts().implicitlyWait(800, TimeUnit.MILLISECONDS);
    }

}
