package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import IO_Resources.*;
import org.openqa.selenium.support.ui.Select;

import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

/**
 * Created by 958105 on 30/04/2018.
 */
public class AuditTaskPage {
    WebDriver driver;
    By audittasknew = By.cssSelector("#hotlist > table > tbody > tr > td.pbButton > input");
    By chorus_task_id = By.id("00N20000002h8ez");
    By Task_audit_statement = By.id("00N20000002h8f8");
    By Task_active_date = By.id("00N20000002h8f7");
    By Task_inactive_date = By.id("00N20000002h8f9");
    By workflow_grp_name = By.id("00N20000009pzLY");
    By section = By.id("00N20000002h8f4");
    By subsection = By.id("00N20000002h8f6");
    By severity = By.id("00N20000002h8f5");
    By audit_save = By.name("name");
    By audit_savenew = By.name("save_new");
    By audit_cancel = By.name("cancel");
    By newsow_task_btn = By.name("new00N20000002h8fX");

    public AuditTaskPage(WebDriver driver){this.driver = driver;}

    public void audittask_new(){
        driver.findElement(audittasknew).click();
        driver.manage().timeouts().implicitlyWait(500,TimeUnit.MILLISECONDS);
    }

    public void audittask_create(Filereaderwrite filehandler){
       driver.findElement(chorus_task_id).sendKeys(filehandler.Chorus_task_id);
       driver.findElement(Task_audit_statement).sendKeys(filehandler.Task_aud_stmt);
       String backdate = DateFunctions.backdate();
       driver.findElement(Task_active_date).sendKeys(backdate);
       String postdays  = DateFunctions.Postdate();
       driver.findElement(Task_inactive_date).sendKeys(postdays);
       Select wkgrp_list = new Select(driver.findElement(workflow_grp_name));
       wkgrp_list.selectByValue("Field Tasks");
       Select section_list = new Select(driver.findElement(section));
       Select subsection_list = new Select(driver.findElement(subsection));
       subsection_list.selectByValue("Equipment");
       section_list.selectByValue("UFB Testing");
       Select severity_list = new Select(driver.findElement(severity));
       severity_list.selectByValue("1");

       driver.findElement(audit_save).click();
       driver.manage().timeouts().implicitlyWait(800,TimeUnit.MILLISECONDS);

    }
    public void audittask_delete()
    {

    }

    public void audittask_edit(){

    }
    public void new_sow_task_click(){
        driver.findElement(newsow_task_btn).click();
        driver.manage().timeouts().implicitlyWait(500,TimeUnit.MILLISECONDS);
    }

}
