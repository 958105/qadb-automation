package Pages;

import IO_Resources.*;
import org.apache.commons.lang3.ObjectUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import java.util.Calendar;

import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;

import static IO_Resources.Filereaderwrite.Service_Partner_name;

/**
 * Created by 958105 on 12/04/2018.
 */
public class SOWServicePartnerPage {
       WebDriver driver;
       Calendar cal = Calendar.getInstance();
       By SP_name_h = By.id("CF00N20000009pzM4");
       By SP_CSA_name_h = By.id("CF00N20000009pzM3");
       By SOW_name_h = By.id("CF00N20000009pzM6");
       By start_date = By.id("00N0O000003rNTe");
       By end_date = By.id("00N0O000003rNTj");
       By save_btn = By.xpath("//*[@id=\"bottomButtonRow\"]/input[1]");
       By saveandnew_btn = By.xpath("//*[@id=\"bottomButtonRow\"]/input[2]");
       By cancel_btn = By.xpath("//*[@id=\"bottomButtonRow\"]/input[3]");
       By SOWSP_id = By.id("Name_ileinner");
       String SOW_Service_Partner_ID;
       By SOWSP_error_list = By.id("errorDiv_ep");
       By SOWSP_List = By.id("CF00N20000009pzM4_lkid");
       By sowsp_edit = By.cssSelector("#topButtonRow > input:nth-child(3)");
       By sowsp_delete = By.cssSelector("#topButtonRow > input:nth-child(4)");
       By sowsp_clone = By.cssSelector("#topButtonRow > input:nth-child(5)");
       By jobQuantities_sp_btn = By.name("newjobquantity");
       By jobQuantities_ch_btn = By.name("new_job_quantity_chorus");
       By job_yr = By.id("j_id0:j_id2:j_id3:j_id4:j_id5");
       By job_JQ_name = By.id("j_id0:j_id2:j_id3:j_id4:j_id6");
       By job_JQ_audit_freq = By.id("j_id0:j_id2:j_id3:j_id4:j_id8");
       By job_JQ_save_btn = By.name("j_id0:j_id2:j_id3:j_id10:j_id11");
       By job_JQ_cancel_btn = By.name("j_id0:j_id2:j_id3:j_id10:j_id12");
       By job_qu_name = By.xpath("//*[@id=\"Name_ileinner\"]");

    public SOWServicePartnerPage(WebDriver driver)
    {
        this.driver = driver;
    }

    public String new_servicepartner_page(Filereaderwrite filehandler)
    {
        Select sp_drop;
        driver.findElement(SP_name_h).sendKeys(filehandler.Service_Partner_name);
        driver.findElement(SP_CSA_name_h).sendKeys(filehandler.CSA_name);
        driver.findElement(SOW_name_h).clear();
        driver.findElement(SOW_name_h).sendKeys(filehandler.SOW_name);
        String minusthreedays  = DateFunctions.backdate();
        driver.findElement(start_date).sendKeys(minusthreedays);
        String postdays  = DateFunctions.Postdate();
        driver.findElement(end_date).sendKeys(postdays);

        WebElement save_button_sp = driver.findElement(save_btn);
        if (save_button_sp.isDisplayed() && save_button_sp.isEnabled())
        {
            driver.manage().timeouts().implicitlyWait(300, TimeUnit.MILLISECONDS);
            driver.findElement(By.xpath("//*[@id=\"ep\"]/div[2]/div[3]/table/tbody/tr[4]/td[4]")).click();
            driver.findElement(save_btn).click();
            driver.manage().timeouts().implicitlyWait(800, TimeUnit.MILLISECONDS);
            if ("Statement of Work Service Partner".equals(driver.getTitle())) {
                SOW_Service_Partner_ID = driver.findElement(SOWSP_id).getText();
            }
            else
            {
                WebElement error_handler = driver.findElement(SOWSP_error_list);
                if (error_handler.isDisplayed()) {
                    if (driver.findElement(SOWSP_List).isDisplayed())
                    {
                        sp_drop = new Select(driver.findElement(SOWSP_List));
                        sp_drop.selectByVisibleText("Broadspectrum");
                        driver.findElement(save_btn).click();
                        driver.manage().timeouts().implicitlyWait(800, TimeUnit.MILLISECONDS);
                        System.out.println(driver.getTitle());
                        if (driver.getTitle().contains("Statement of Work Service Partner")) {
                            SOW_Service_Partner_ID = driver.findElement(SOWSP_id).getText();
                            System.out.println("Partner ID :" + SOW_Service_Partner_ID);
                        }
                    }
                    else
                        SOW_Service_Partner_ID = null;
                }
                else
                    SOW_Service_Partner_ID = null;
            }


        }else
         SOW_Service_Partner_ID = null;

        return SOW_Service_Partner_ID;
    }

    public void delete_SOWSP(String sow_sp)
    {
        driver.findElement(sowsp_delete).click();
        Alert alert = driver.switchTo().alert();
        alert.accept();
        driver.manage().timeouts().implicitlyWait(200, TimeUnit.MILLISECONDS);

    }
    public void Job_Quantities_sp_click(){
        driver.findElement(jobQuantities_sp_btn).click();
        driver.manage().timeouts().implicitlyWait(800, TimeUnit.MILLISECONDS);
    }

    public void Job_Quantities_ch_click(){
        driver.findElement(jobQuantities_ch_btn).click();
        driver.manage().timeouts().implicitlyWait(800, TimeUnit.MILLISECONDS);
    }

    public void job_quantities_sp_create(String Year, String JQ, String JQ_freq, Filereaderwrite filehandler )
    {
          driver.findElement(job_yr).sendKeys(Year);
          driver.findElement(job_JQ_name).sendKeys(JQ);
          driver.findElement(job_JQ_audit_freq).sendKeys(JQ_freq);
          driver.findElement(job_JQ_save_btn).click();
          driver.manage().timeouts().implicitlyWait(800, TimeUnit.MILLISECONDS);
          filehandler.Job_Q_ID = driver.findElement(job_qu_name).getText();
          System.out.println("Job Quantaties " + filehandler.Job_Q_ID);
    }

    public void job_quantities_delete(String JQ_name){

    }

}
