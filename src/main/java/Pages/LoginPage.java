package Pages;

/**
 * Created by 958105 on 23/03/2018.
 */
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class LoginPage {
    WebDriver driver;
    By username = By.name("username");
    By Password = By.name("pw");
    By TitleTest = By.tagName("title");
    By Login = By.id("Login");


    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    // set value methods
    public void setUsernamevalueloginpage(String  strUsername){
        driver.findElement(username).sendKeys(strUsername);
    }

    public void setPasswordvalueloginpage(String strPassword){
        driver.findElement(Password).sendKeys(strPassword);
    }

    public void clickLogin(){
        WebElement button = driver.findElement(Login);
        button.click();
        // implicit wait (have to change it to explicit wait function
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

    }

    public String getLoginTitle(){
        return driver.getTitle();
    }


    public void loginToSalesforece(String strUserName,String strPassword){
     /* moved to Test Class
     driver.get("https://test.salesforce.com/"); */
        //Fill user name
        this.setUsernamevalueloginpage(strUserName);
        //Fill password
        this.setPasswordvalueloginpage(strPassword);
        //Click Login button
        this.clickLogin();



    }

}
